import React,{Component} from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { Layout, Menu, Icon, Tooltip } from 'antd';
import 'antd/dist/antd.css';
import './App.css';
import Auth           from './auth';
import Account        from './components/account';
import Chatlist       from './components/chatlist';
import Chat           from './components/chat';
import Friends        from './components/friends';
import SearchFriends  from './components/searchFriends';
import Mapfriends     from './components/mapfriends';

const { Header, Content, Sider, Footer } = Layout;

class App extends Component {
  async componentDidMount(){
    if (!Auth.loggedIn()) {
      this.props.history.replace('/login');
      return;
    }
  }
  onLogout = (e) => {
    e.preventDefault();
    // Here send login request
    Auth.logout();
    this.props.history.replace('/login');
  }
  isMobileDevice = () => {
    var check = false;
    (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw(n|u)|c55\/|capi|ccwa|cdm|cell|chtm|cldc|cmd|co(mp|nd)|craw|da(it|ll|ng)|dbte|dcs|devi|dica|dmob|do(c|p)o|ds(12|d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(|_)|g1 u|g560|gene|gf5|gmo|go(\.w|od)|gr(ad|un)|haie|hcit|hd(m|p|t)|hei|hi(pt|ta)|hp( i|ip)|hsc|ht(c(| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i(20|go|ma)|i230|iac( ||\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|[a-w])|libw|lynx|m1w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|mcr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|([1-8]|c))|phil|pire|pl(ay|uc)|pn2|po(ck|rt|se)|prox|psio|ptg|qaa|qc(07|12|21|32|60|[2-7]|i)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h|oo|p)|sdk\/|se(c(|0|1)|47|mc|nd|ri)|sgh|shar|sie(|m)|sk0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h|v|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl|tdg|tel(i|m)|tim|tmo|to(pl|sh)|ts(70|m|m3|m5)|tx9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas|your|zeto|zte/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
    return check;
  };
  renderMobile = () =>{
    return (
      <Router>
          <Layout>
              <Header style={{ position: 'fixed', zIndex: 1, background: 'CornflowerBlue', 
                               width: '100%', textAlign:"center", height: "30px"}}>
                  <span>Social network</span>
                  <span>
                  <Tooltip placement="leftBottom" title='Выйти'>
                    <Icon 
                        type='logout'
                        style={{ cursor: 'pointer', float: 'right', paddingTop: "5px", paddingRight:"10px"}}
                        onClick={this.onLogout}
                    />
                  </Tooltip>
                  </span>
              </Header>
              <Layout style={{marginTop: "80px"}}>
                    <Content style={{ background: '#fff', height: '100%', maxWidth: '600px', minWidth:'300px'}}>
                        <Route exact path="/"                  component={Account}/>
                        <Route exact path="/chatlist"          component={Chatlist}/>
                        <Route exact path="/friends"           component={Friends}/>
                        <Route exact path="/searchfriends"     component={SearchFriends}/>
                        <Route       path="/friends/:id"       component={Account}/>
                        <Route       path="/searchfriends/:id" component={Account}/>
                        <Route       path="/chatlist/:id"      component={Chat}/>
                        <Route exact path="/mapfriends"        component={Mapfriends}/>
                    </Content>
              </Layout>
              <Footer style={{ position: 'fixed', marginTop: "35px", padding: 0, textAlign:"center", width: "100%", height: "30px"}}>
                <Menu selectable={false} mode="horizontal">
                  <Menu.Item>
                    <Icon type="home"/>
                    <Link to="/" />
                  </Menu.Item>
                  <Menu.Item>
                    <Icon type="message" />
                    <Link to="/chatlist" />
                  </Menu.Item>
                  <Menu.Item>
                    <Icon type="user" />
                    <Link to="/friends" />
                  </Menu.Item>
                  <Menu.Item>
                    <Icon type="search" />
                    <Link to="/searchfriends" />
                  </Menu.Item>
                  <Menu.Item>
                    <Icon type="global" />
                    <Link to="/mapfriends" />
                  </Menu.Item>
                </Menu>
              </Footer>
          </Layout>
      </Router>
    );
  }
  renderDesktop = () =>{
    return (
      <Router>
          <Layout>
              <Header style={{ position: 'fixed', zIndex: 1, background: 'CornflowerBlue', 
                               width: '100%', textAlign:"center", height: "30px"}}>
                  <span>Social network</span>
                  <span>
                  <Tooltip placement="leftBottom" title='Выйти'>
                    <Icon 
                        type='logout'
                        style={{ cursor: 'pointer', float: 'right', paddingTop: "5px", paddingRight:"10px"}}
                        onClick={this.onLogout}
                    />
                  </Tooltip>
                  </span>
              </Header>
              <Layout style={{marginTop: "35px", marginLeft:'10px'}}>
                  <Sider style={{ background: '#fff'}}>
                    <Menu selectable={false}>
                      <Menu.Item>
                        <Icon type="home"/>
                        <span>Моя страница</span>
                        <Link to="/" />
                      </Menu.Item>
                      <Menu.Item>
                        <Icon type="message" />
                        <span>Сообщения</span>
                        <Link to="/chatlist" />
                      </Menu.Item>
                      <Menu.Item>
                        <Icon type="user" />
                        <span>Друзья</span>
                        <Link to="/friends" />
                      </Menu.Item>
                      <Menu.Item>
                        <Icon type="search" />
                        <span>Поиск друзей</span>
                        <Link to="/searchfriends" />
                      </Menu.Item>
                      <Menu.Item>
                        <Icon type="global" />
                        <span>Друзья на карте</span>
                        <Link to="/mapfriends" />
                      </Menu.Item>
                    </Menu>
                  </Sider>
                  <Layout>
                    <Content style={{ background: '#fff', height: '100%', 
                                      marginLeft:'20px', maxWidth: '600px', minWidth:'400px'}}>
                        <Route exact path="/"                  component={Account}/>
                        <Route exact path="/chatlist"          component={Chatlist}/>
                        <Route exact path="/friends"           component={Friends}/>
                        <Route exact path="/searchfriends"     component={SearchFriends}/>
                        <Route       path="/friends/:id"       component={Account}/>
                        <Route       path="/searchfriends/:id" component={Account}/>
                        <Route       path="/chatlist/:id"      component={Chat}/>
                        <Route exact path="/mapfriends"        component={Mapfriends}/>
                    </Content>
                  </Layout>
              </Layout>
          </Layout>
      </Router>
    );
  }
  render(){
    if (this.isMobileDevice())
      return this.renderMobile();
    else
      return this.renderDesktop();
    
  };
}
export default App;
