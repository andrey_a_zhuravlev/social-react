import decode from 'jwt-decode';

const nameToken = "token";
const nameId    = "id";
const nameEmail = "email";

//export const ServerAddress     = "https://nameless-ridge-54738.herokuapp.com/";
//export const ServerChatAddress = "https://nameless-ridge-54738.herokuapp.com/";
export const ServerAddress     = "http://localhost:3001/";
export const ServerChatAddress = "http://localhost:3001/";
export const ServerApiAddress  = ServerAddress+"api";

export default class Auth {
    // Checks if there is a saved token and it's still valid
    static loggedIn() {
        const token = Auth.getToken();
        return !!token && !Auth.isTokenExpired(token); 
    }
    // Checking if token is expired
    static isTokenExpired(token) {
        try {
            const decoded = decode(token);
            if (decoded.exp < Date.now() / 1000) {
                return true;
            }
            else
                return false;
        } catch (err) {
            console.log("expired check failed!");
            return false;
        }
    }
    // Saves user token and profile data to localStorage
    static setUser(user) {
        localStorage.setItem(nameToken, user.token);
        localStorage.setItem(nameId,    user.id);
        localStorage.setItem(nameEmail, user.email);
    }
    // Retrieves the user token from localStorage
    static getToken() {
        return localStorage.getItem(nameToken);
    }
    // Retrieves the user id from localStorage
    static getId() {
        return localStorage.getItem(nameId);
    }
    // Retrieves the user email from localStorage
    static getEmail() {
        return localStorage.getItem(nameEmail);
    }
    // Clear user token and profile data from localStorage
    static logout() {
        localStorage.removeItem(nameToken);
        localStorage.removeItem(nameId);
        localStorage.removeItem(nameEmail);
    }
    // Using jwt-decode npm package to decode the token
    static getConfirm() {
        let answer = decode(Auth.getToken());
        return answer;
    }
    // Get Header config with Auth token
    static getAuthCfg() {
        return  {headers: { Authorization: Auth.getToken()}};
    }
    static checkResponseStatus(response) {
        // raises an error in case response status is not a success
        if (response.status >= 200 && response.status < 300) { // Success status lies between 200 to 300
            return response
        } else {
            var error = new Error(response.statusText)
            error.response = response
            throw error
        }
    }
};