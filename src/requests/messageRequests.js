import axios from "axios";
import Auth from "../auth";

export default class MessageRequests {
    static async get(chatId) {
        try {
            const response = await axios.get(`/message.get?chatId=${chatId}`, Auth.getAuthCfg());
            Auth.checkResponseStatus(response);
            return response.data;
        } catch(error){
            console.log(`message.get request error ${error}`);
            return false;
        }
    }
}