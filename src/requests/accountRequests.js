import axios from "axios";
import Auth from "../auth";

export default class AccountRequests {
    static async login(email, password) {
        try {
            const url = `/account.login?email=${email}&password=${password}`;
            var response = await axios.post(url);
            Auth.checkResponseStatus(response);
            Auth.setUser(response.data.user);
            return true;
        } catch(error){
            console.log(`account.login request error ${error}`);
            return false;
        }
    }
    static async fblogin(fbtoken) {
        try {
            const data= {"access_token": fbtoken}
            const response = await axios.post(`/account.fblogin`, data);
            Auth.checkResponseStatus(response);
            Auth.setUser(response.data.user);
            return true;
        } catch(error){
            console.log(`account.fblogin request error ${error}`);
            return false;
        }
    }
    static async signup(firstname, secondname, email, password) {
        try {
            const url = `/account.register?email=${email}&password=${password}&firstname=${firstname}&secondname=${secondname}`;
            const response = await axios.post(url);
            Auth.checkResponseStatus(response);
            return true;
        } catch(error){
            console.log(`account.signup request error ${error}`);
            return false;
        }
    }
    static async getInfo(id) {
        try {
            const response = await axios.get(`/account.getInfo?id=${id}`, Auth.getAuthCfg());
            return response.data;
        } catch(error){
            console.log(`account.getInfo request error ${error}`);
            return false;
        }
    }
    static async uploadAvatar(options) {
        try {
            const data= new FormData()
            data.append('avatar', options.file)
            const config= {
                "headers": {
                "content-type": 'multipart/form-data; boundary=----WebKitFormBoundaryqTqJIxvkWFYqvP5s',
                "Authorization": Auth.getToken(),
                }
            }
            const response = await axios.post(`/account.uploadAvatar`, data, config);
            options.onSuccess(response.data, options.file)
            return response.data;
        } catch(error){
            console.log(`account.uploadAvatar request error ${error}`);
            return false;
        }
    }
    static async updateInfo(firstname, secondname) {
        try {
            const data= {
                "firstname": firstname,
                "secondname": secondname
            }
            await axios.post(`/account.updateInfo`, data, Auth.getAuthCfg());
        } catch(error){
            console.log(`account.updateInfo request error ${error}`);
        }
    }
    static async setPosition(lat, lng) {
        try {
            const data= {
                "lat": lat,
                "lng": lng
            }
            //console.log(`account.setPosition ${JSON.stringify(data)}`);
            await axios.post(`/account.setPosition`, data, Auth.getAuthCfg());
        } catch(error){
            console.log(`account.setPosition request error ${error}`);
        }
    }
}