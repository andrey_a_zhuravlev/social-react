import axios from "axios";
import Auth from "../auth";

export default class FriendRequests {
    static async search(email) {
        try {
            const response = await axios.get(`/friends.search?email=${email}`, Auth.getAuthCfg());
            Auth.checkResponseStatus(response);
            return response.data;
        } catch(error){
            console.log(`friends.search request error ${error}`);
            return false;
        }
    }
    static async getList() {
        try {
            const response = await axios.get(`/friends.getList`, Auth.getAuthCfg());
            Auth.checkResponseStatus(response);
            return response.data;
        } catch(error){
            console.log(`friends.getList request error ${error}`);
            return false;
        }
    }
    static async getMapList() {
        try {
            const response = await axios.get(`/friends.getMapList?radius=10000`, Auth.getAuthCfg());
            Auth.checkResponseStatus(response);
            return response.data;
        } catch(error){
            console.log(`friends.getMapList request error ${error}`);
            return false;
        }
    }
    static async sendFriendRequest(id) {
        try {
            await axios.post(`/friends.addFriend?id=${id}`, null, Auth.getAuthCfg());
        } catch(error){
            console.log(`friends.addFriend request error ${error}`);
        }
    }
    static async sendDeleteFriendRequest(id) {
        try {
            await axios.post(`/friends.deleteFriend?id=${id}`, null, Auth.getAuthCfg());
        } catch(error){
            console.log(`friends.deleteFriend request error ${error}`);
        }
    }
}