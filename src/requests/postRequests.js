import axios from "axios";
import Auth from "../auth";

export default class PostRequests {
    static async getPosts(id) {
        try {
            const response = await axios.get(`/post.getPosts?id=${id}`, Auth.getAuthCfg());
            return response.data;
        } catch(error){
            console.log(`post.getPosts request error ${error}`);
            return false;
        }
    }
    static async addComment(postId, text) {
        try {
            const data = {
                "postId": postId,
                "body": text
            }
            await axios.post(`/comment.add`, data, Auth.getAuthCfg());
        } catch(error){
            console.log(`comment.add request error ${error}`);
        }
    }
    static async addPost(header, body) {
        try {
            const data = {
                "header": header,
                "body": body
            }
            await axios.post(`/post.add`, data, Auth.getAuthCfg());
        } catch(error){
            console.log(`post.add request error ${error}`);
        }
    }
    static async changePost(postId, header, body) {
        try {
            const data = {
                "postId": postId,
                "header": header,
                "body": body
            }
            await axios.post(`/post.change`, data, Auth.getAuthCfg());
        } catch(error){
            console.log(`post.change request error ${error}`);
        }
    }
    static async deletePost(postId) {
        try {
            await axios.post(`/post.delete?postId=${postId}`, null, Auth.getAuthCfg());
        } catch(error){
            console.log(`post.delete request error ${error}`);
        }
    }
}