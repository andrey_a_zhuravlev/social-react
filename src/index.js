import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';
import Login from './components/login';
import Signup from './components/signup';

import "bootstrap/dist/css/bootstrap.css";

import {ServerApiAddress} from "./auth";
import axios from "axios";

// We need setup this once
axios.defaults.baseURL = ServerApiAddress;
axios.defaults.headers.post['Content-Type'] = 'application/json';
axios.defaults.headers.get['Content-Type']  = 'application/json';

ReactDOM.render(
    <Router>
        <div>
            <Route exact path="/"               component={App} />
            <Route exact path="/chatlist"       component={App} />
            <Route       path="/friends"        component={App} />
            <Route       path="/searchfriends"  component={App} />
            <Route exact path="/login"          component={Login} />
            <Route exact path="/signup"         component={Signup} />
        </div>
    </Router>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
