import React from 'react';
import Friends from '../components/friends';
import Enzyme, { shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import Adapter from 'enzyme-adapter-react-16';
Enzyme.configure({ adapter: new Adapter() });

jest.mock('../requests/friendRequests');

beforeEach(() => {
  // Clear all instances and calls to constructor and all methods:
});

it('Friends renders correctly with test data. Shallow', async (done) => {
  const output = await shallow( <Friends/> );
  const instance = output.instance();
  await instance.componentDidMount();
  expect(shallowToJson(output)).toMatchSnapshot();
  done();
});