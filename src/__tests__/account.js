import React from 'react';
import Account from '../components/account';
import Enzyme, { shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import Adapter from 'enzyme-adapter-react-16';
import Item from 'antd/lib/list/Item';
Enzyme.configure({ adapter: new Adapter() });

jest.mock('../requests/accountRequests');
jest.mock('../requests/postRequests');

beforeEach(() => {
  // Clear all instances and calls to constructor and all methods:
  //AccountRequests.mockClear();
  //PostRequests.mockClear();
});

describe('Account tests', () =>{
  
  it('Account renders correctly with test data. MatchSnapshot', async (done) => {
    const output = await shallow( <Account/> );
    const instance = output.instance();
    await instance.componentDidMount();
    expect(shallowToJson(output)).toMatchSnapshot();
    done();
  });

  it('Change value of changePostHeader and changePostBody', async (done) => {
    const output = await shallow( <Account/> );
    const instance = output.instance();
    await instance.componentDidMount();
    
    var header = 'Some new header';
    var body = 'Some new body';
    expect(output.state('changePostHeader')).toEqual(undefined);
    expect(output.state('changePostBody')).toEqual(undefined);

    output.find('Input').find({name:"changePostHeader"}).simulate('change', 
                                    { target: { 
                                        name:'changePostHeader', 
                                        value: header} });
    expect(output.state('changePostHeader')).toEqual(header);

    output.find('TextArea').find({name:"changePostBody"}).simulate('change', 
                                    { target: { 
                                        name:'changePostBody', 
                                        value: body} });
    expect(output.state('changePostBody')).toEqual(body);

    done();
  });

  it('Change value of changeInfoFirstname and changeInfoSecondname', async (done) => {
    const output = await shallow( <Account/> );
    const instance = output.instance();
    await instance.componentDidMount();
    
    var firstName = 'Aname';
    var secondName = 'Asecondnam';
    
    expect(output.state('changeInfoFirstname')).toEqual(undefined);
    expect(output.state('changeInfoSecondname')).toEqual(undefined);

    output.find('Input').find({name:"changeInfoFirstname"}).simulate('change', 
                                    { target: { 
                                        name:'changeInfoFirstname', 
                                        value: firstName} });
    expect(output.state('changeInfoFirstname')).toEqual(firstName);

    output.find('Input').find({name:"changeInfoSecondname"}).simulate('change', 
                                    { target: { 
                                        name:'changeInfoSecondname', 
                                        value: secondName} });
    expect(output.state('changeInfoSecondname')).toEqual(secondName);
    done();
  });
});