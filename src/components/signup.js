import React,{Component} from "react";
import './css/login.css'
import 'antd/dist/antd.css';
import { Button, Input, Form, Typography} from 'antd';
import AccountRequests from '../requests/accountRequests';
import Auth from "../auth";

export default class Signup extends Component {
    constructor (props) {
        super(props);
        this.state = {
            email: "",
            password: "",
            firstname: "",
            secondname: ""};
    }
    onInputChange = (e)=>{
        this.setState({[e.target.name]:e.target.value});
    }
    onSignup = async (e)=>{
        e.preventDefault();
        // Here send sign up request
        let res = await AccountRequests.signup(this.state.firstname, 
                                         this.state.secondname, 
                                         this.state.email, 
                                         this.state.password);
        if (res === false) {
            return alert("Error to register new user!");
        }
        this.props.history.replace('/login');
    }
    componentDidMount(){
        //if logedin allready 
        if (Auth.loggedIn())
          this.props.history.replace('/');
    }
    render(){
        return (
            <Form className="login-form">
                <h4 className="text-center"> Register to </h4>
                <h3 className="text-center"> Social network </h3>
                <Typography>Email</Typography>
                <Input type="email" className="login-form-input" name="email" placeholder="Your Email" onChange={this.onInputChange}/>
                <Typography>Password</Typography>
                <Input type="password" className="login-form-input" name="password" placeholder="Your password" onChange={this.onInputChange}/>

                <Typography>First name</Typography>
                <Input type="text" name="firstname" placeholder="Your first name" onChange={this.onInputChange}/>
                <Typography>Second name</Typography>
                <Input type="text" name="secondname" placeholder="Your second name" onChange={this.onInputChange}/>
                <Button type="primary" className="login-form-button" onClick={this.onSignup} block>
                    Create account
                </Button>
                <div className="text-center">
                    <a href="/login">I have account!</a>
                </div>
            </Form>
        );
    }
}