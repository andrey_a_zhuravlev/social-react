import React,{Component} from "react";
import AccountRequests from '../requests/accountRequests';
import PostRequests from '../requests/postRequests';
import { Input, Comment, Form,List,  Divider, 
         Button, Popconfirm, Card, Icon, Avatar,
         Upload, Tooltip, Drawer, message } from 'antd';
import './css/account.css';
import Auth, {ServerAddress} from "../auth";

const { Meta } = Card;
const { TextArea } = Input;

export default class Account extends Component {
    userId = null;
    constructor (props) {
        super(props);
        this.state ={changePostVisible: false,
                     changeInfoVisible: false};
        if (this.props.match !== undefined &&
            this.props.match.params !== undefined &&
            this.props.match.params.id !== undefined)
            this.userId = this.props.match.params.id;
    }
    beforeUpload = (file) => {
        const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
        if (!isJpgOrPng) {
            message.error('You can upload only JPG/PNG file!');
        }
        const isLt2M = file.size / 1024 / 1024 < 2;
        if (!isLt2M) {
            message.error('Image must smaller than 2MB!');
        }
        return isJpgOrPng && isLt2M;
    }
        
    uploading = info => {
        if (info.file.status === 'done') {
            message.success(`${info.file.name} file uploaded successfully.`);
            this.setState({updatetmp:true});
        } else if (info.file.status === 'error') {
            message.error(`${info.file.name} file upload failed.`);
        }
    };
    uploadAvatar = async (options) =>{
        const data = await AccountRequests.uploadAvatar(options);
        if (!!data === true)
            this.setState(data);
    }
    onChangeInput = (e) => {
        this.setState({[e.target.name]:e.target.value});
    };
    onAddPost = async () => {
        await PostRequests.addPost(this.state.postHeader, this.state.postBody);
        const posts = await PostRequests.getPosts(this.userId);
        this.setState({posts: posts});
        this.setState({postHeader: ''});
        this.setState({postBody: ''});
    }
    onAddComment = async (postId) => {
        await PostRequests.addComment(postId, this.state["addComment"+postId]);
        const posts = await PostRequests.getPosts(this.userId);
        this.setState({posts: posts});
        this.setState({["addComment"+postId]: ''});
    }
    onEditPost = async (postId) => {
        await PostRequests.addComment(postId, this.state["addComment"+postId]);
        const posts = await PostRequests.getPosts(this.userId);
        this.setState({posts: posts});
    }
    onDeletePost = async (postId) => {
        await PostRequests.deletePost(postId);
        const posts = await PostRequests.getPosts(this.userId);
        this.setState({posts: posts});
    }
    async componentDidMount(){
        const data = await AccountRequests.getInfo(this.userId);
        if (!!data === true)
            this.setState(data);
        const posts = await PostRequests.getPosts(this.userId);
        if (posts !== false)
            this.setState({posts: posts});
    }
    renderComments = (comments) => {
        if (comments.length === 0)
            return (null);
        return (
            <List
                size="small"
                itemLayout="vertical"
                dataSource={comments}
                renderItem={comment => (
                    <List.Item style={{paddingTop:0, paddingBottom:0, borderBottomWidth:0}}>
                        <Comment style={{paddingTop:0, paddingBottom:0}}
                            author={
                                <div>
                                    <a href={"/friends/"+comment.user.id}>
                                        {comment.user.firstname + " " + comment.user.secondname}
                                    </a>
                                </div>
                            }
                            avatar={
                                <Avatar
                                src={ServerAddress + comment.user.avatar+"_small"}
                                alt={"Avatar"}
                                />
                            }
                            content={<p>{comment.body}</p>}/>
                    </List.Item>
                )}/>
        );
    }
    onSendMessage = () => {
        this.props.history.replace('/chatlist/'+this.userId);
    }
    renderAccountInfo  = () => {
        var actions = [];
        if (this.userId === null) {
            actions = [
                <Tooltip placement="top" title='Редактировать профиль'>
                    <Icon type="edit" key="edit" onClick={() => this.showChangeInfo()}/>
                </Tooltip>,
                <Upload listType  = "picture"
                        className = "avatar-uploader"
                        showUploadList = {false}
                        beforeUpload  = {this.beforeUpload}
                        onChange      = {this.uploading}
                        customRequest = {this.uploadAvatar}>
                        <Tooltip placement="top" title='Изменить аватар'>
                            <Icon type="file-image" /> 
                        </Tooltip>
                </Upload>
            ];
        } else {
            actions = [
                <Button htmlType="submit" onClick={this.onSendMessage} 
                        type="primary" style={{float: "right"}}>
                Написать сообщение
                </Button>
            ];
        }
        return (
            <div>
            <Card 

                actions={actions}>
                <Meta style={{ paddingTop: "20px" }}
                avatar={<img style={{width: "150px"}} alt="avatar" 
                                src={ServerAddress + this.state.avatar}/>}
                
                title={this.state.firstname + " " + this.state.secondname}
                description={this.state.email} />
            </Card>
            <Divider style={{height:"4px"}}/>
            </div>
        );
    }
    renderAddPostArea = () => {
        if (this.userId !== null) {
            return (<div></div>);
        }
        return (
            <div>
            <Form.Item style={{paddingLeft: "10px", paddingRight: "10px"}}>
                <Input name="postHeader" 
                       value={this.state.postHeader}
                       onChange={this.onChangeInput} 
                       placeholder="Тема вашего поста"/>
                <TextArea 
                        name="postBody"
                        value={this.state.postBody}
                        autosize={{ minRows: 2}} 
                        onChange={this.onChangeInput}
                        placeholder="Что вы хотите написать?"/>
            </Form.Item>
            <Form.Item style={{paddingLeft: "10px", paddingRight: "10px"}}>
                <Button htmlType="submit" onClick={this.onAddPost} 
                        type="primary" style={{float: "right"}}>
                Опубликовать пост
                </Button>
            </Form.Item>
            <Divider style={{height:"4px"}}/>
            </div>
        );
    }
    showChangePost = (post) => {
        this.setState({ changePostBody:    post.body });
        this.setState({ changePostHeader:  post.header });
        this.setState({ changePostId:      post.id });
        this.setState({ changePostVisible: true });
    };
    onCloseChangePost = () => {
        this.setState({ changePostVisible: false });
    };
    onApplyChangePost = async () => {
        await PostRequests.changePost(this.state.changePostId,
                                      this.state.changePostHeader, 
                                      this.state.changePostBody);
        this.setState({ changePostVisible: false });
        const posts = await PostRequests.getPosts(this.userId);
        this.setState({posts: posts});
    };

    showChangeInfo = () => {
        this.setState({ changeInfoFirstname:  this.state.firstname });
        this.setState({ changeInfoSecondname: this.state.secondname });
        this.setState({ changeInfoVisible: true });
    };
    onCloseChangeInfo = () => {
        this.setState({ changeInfoVisible: false });
    };
    onApplyChangeInfo = async () => {
        await AccountRequests.updateInfo(this.state.changeInfoFirstname,
                                         this.state.changeInfoSecondname);
        this.setState({ changeInfoVisible: false });
        const data = await AccountRequests.getInfo(this.userId);
        if (!!data === true)
            this.setState(data);
        const posts = await PostRequests.getPosts(this.userId);
        if (posts !== false)
            this.setState({posts: posts});
    };
    renderChangeInfo = () => {
        return (
            <div>
                <Drawer
                    placement="right"
                    title="Редактирование аккаунта"
                    onClose={this.onCloseChangeInfo}
                    visible={this.state.changeInfoVisible}>
                    <div
                        style={{
                        position: 'absolute',
                        left: 0,
                        width: '100%',
                        background: '#fff',
                        textAlign: 'right',
                        }}>
                        <Form.Item style={{paddingLeft: "10px", paddingRight: "10px"}}>
                            <Input 
                                    name="changeInfoFirstname" 
                                    value={this.state.changeInfoFirstname}
                                    onChange={this.onChangeInput} 
                                    placeholder="Имя"/>
                            <Input 
                                    name="changeInfoSecondname" 
                                    value={this.state.changeInfoSecondname}
                                    onChange={this.onChangeInput} 
                                    placeholder="Фамилия"/>
                            <Button onClick={this.onCloseChangeInfo} style={{ marginRight: "8px"}}>
                            Отмена
                            </Button>
                            <Button onClick={this.onApplyChangeInfo} type="primary" style={{ marginRight: "8px"}}>
                            Сохранить
                            </Button>
                        </Form.Item>
                    </div>
                </Drawer>
            </div>
        );
    }

    renderChangePost = () => {
        return (
            <div>
                <Drawer
                    placement="bottom"
                    title="Редактирование поста"
                    onClose={this.onCloseChangePost}
                    visible={this.state.changePostVisible}>
                    <div
                        style={{
                        position: 'absolute',
                        left: 0,
                        bottom: 0,
                        width: '100%',
                        background: '#fff',
                        textAlign: 'right',
                        }}>
                        <Form.Item style={{paddingLeft: "10px", paddingRight: "10px"}}>
                            <Input 
                                    name="changePostHeader" 
                                    value={this.state.changePostHeader}
                                    onChange={this.onChangeInput} 
                                    placeholder="Тема вашего поста"/>
                            <TextArea 
                                    name="changePostBody"
                                    value={this.state.changePostBody}
                                    autosize={{ minRows: 4}} 
                                    onChange={this.onChangeInput}
                                    placeholder="Что вы хотите написать?"/>
                            <Button onClick={this.onCloseChangePost} style={{ marginRight: "8px"}}>
                            Отмена
                            </Button>
                            <Button onClick={this.onApplyChangePost} type="primary" style={{ marginRight: "8px"}}>
                            Сохранить
                            </Button>
                        </Form.Item>
                    </div>
                </Drawer>
            </div>
        );
    }

    getPostActions = (post) => {
        const actions = [
            <Icon type="edit" key="edit" onClick={() => this.showChangePost(post)} style={{width:"40px"}}/>,
            <Popconfirm
                    style={{width:"40px"}}
                    title="Are you sure whant to delete this post?"
                    onConfirm={() => this.onDeletePost(post.id)}
                    okText="Yes"
                    cancelText="No">
                <Icon type="delete"/>
            </Popconfirm>
        ];
        if (parseInt(Auth.getId()) === post.userId) {
            return actions;
        } else {
            return [];
        }
    }
    renderPost = (post) => {
        return (
            <Comment style={{paddingBottom:0}}
                    actions={this.getPostActions(post)}
                    author={
                        <div>
                            <a href={"/friends/"+this.state.id}>
                                {this.state.firstname + " " + this.state.secondname + ":  "}
                            </a>
                            {post.header}
                        </div>
                    }
                    avatar={<Avatar src={ServerAddress + this.state.avatar+"_small"}/>}
                    content={<p>{post.body}</p>}>
                {this.renderComments(post.comments)}
            </Comment>
        );
    }
    renderAllPost = () => {
        return (
            <div>
                <List
                    size="small"
                    itemLayout="vertical"
                    dataSource={this.state.posts}
                    renderItem={post => (
                    <List.Item> 
                        {this.renderPost(post)}
                        <div style={{marginRight:'5px'}}>
                            <Input onChange={this.onChangeInput} 
                                    style={{ width: '80%' }} 
                                    name={"addComment"+post.id}
                                    value={this.state["addComment"+post.id]}
                                    placeholder="Написать комментарий..."/>
                            <Button style={{ width: '20%', paddingLeft: "2px", paddingRight:"2px"}} onClick={() => this.onAddComment(post.id)} type="primary">
                                    Ответить
                            </Button>
                        </div>
                    </List.Item>
                    )}/>
                <Divider style={{height:"4px"}}/>
            </div>
        );
    }
    render(){
        return (
            <div style={{minHeight: "calc(100vh - 35px)"}}>
                {this.renderChangeInfo()}
                {this.renderChangePost()}
                {this.renderAccountInfo()}
                {this.renderAddPostArea()}
                {this.renderAllPost()}
            </div>
        )
    };
}
