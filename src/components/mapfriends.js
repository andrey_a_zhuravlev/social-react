import React,{Component, createRef} from "react";
import { Button } from 'antd';
import AccountRequests from '../requests/accountRequests';
import FriendRequests from '../requests/friendRequests';

const API_KEY = "AIzaSyABe8eXvap863Qz4y-LPzC-UDUiNqY0RM8";

export default class Mapfriends extends Component {
    googleScript = null;
    googleMapRef = createRef();
  constructor (props) {
    super(props);
    this.state = {};
    this.state.markers = [];
    this.state.lat = 0.0;
    this.state.lng = 0.0;
  }
  async componentDidMount() {
    const data = await AccountRequests.getInfo(null);
    if (!!data === true) {
        this.setState(data);
    }
    this.initGoogleMap();
  }
  componentWillUnmount(){
    window.document.body.removeChild(this.googleScript);
    this.googleScript.remove();
    window.google = undefined;
  }
  initGoogleMap = () => {
    this.googleScript = document.createElement('script');
    this.googleScript.src = `https://maps.googleapis.com/maps/api/js?key=${API_KEY}&libraries=places`;
    window.document.body.appendChild(this.googleScript);
    this.googleScript.addEventListener('load', this.onLoadGoogleMap);
  }
  onLoadGoogleMap = async () => {
    this.googleMap = this.createGoogleMap();
    this.googleMap.addListener('click', this.onAddMarker);
    await this.addAllMarkers();
    
  }
  createGoogleMap = () => {
    return new window.google.maps.Map(this.googleMapRef.current, {
        zoom:10,
        center:{ lat: 47.238926, lng: 38.878932 },
        disableDefaultUI: true
    });
  }
  onAddMarker = async (event) => {
    this.deleteMarkers();
    this.state.lat = event.latLng.lat();
    this.state.lng = event.latLng.lng();
    this.addOwnMarker();
  }
  addOwnMarker = () => {
    this.addMarker({lat: this.state.lat, lng: this.state.lng}, "Вы", true);
  }
  // Adds a marker to the map and push to the array.
  addMarker = (location, str, isOwnMarker) => {
    var iconUrl = "http://maps.google.com/mapfiles/ms/icons/red-dot.png";
    if (isOwnMarker === true) {
      iconUrl = "http://maps.google.com/mapfiles/ms/icons/blue-dot.png";
    }
    var marker = new window.google.maps.Marker({
      position: location,
      map: this.googleMap,
      icon: {url: iconUrl}
    });
    var infowindow = new window.google.maps.InfoWindow({
      content: str
    });
    marker.addListener('click', function() {
      infowindow.open(this.googleMap, marker);
    });
    this.state.markers.push(marker);
  }

  // Sets the map on all markers in the array.
  setMapOnAll = (map) => {
    for (let i = 0; i < this.state.markers.length; i++) {
      this.state.markers[i].setMap(map);
    }
  }

  // Removes the markers from the map, but keeps them in the array.
  clearMarkers = () => {
    this.setMapOnAll(null);
  }

  // Shows any markers currently in the array.
  showMarkers = () => {
    this.setMapOnAll(this.googleMap);
  }

  // Deletes all markers in the array by removing references to them.
  deleteMarkers = () => {
    this.clearMarkers();
    this.setState({markers:[]});
  }
  addAllMarkers = async () => {
    this.deleteMarkers();
    this.addOwnMarker();
    let mapFriendList = await FriendRequests.getMapList();
    if (mapFriendList !== false) {
      this.setState({list:mapFriendList});
      for (let i = 0; i < mapFriendList.length; i++) {
        let str = '<div>' + 
                    '<a href="/friends/' + mapFriendList[i].id + '">' +
                    mapFriendList[i].firstname + " " + mapFriendList[i].secondname +
                    '</a>'+
                  '</div>';
        this.addMarker({lat: mapFriendList[i].lat, lng: mapFriendList[i].lng}, str, false);
      }
    }
  }
  onSetPosition = async () => {
    await AccountRequests.setPosition(this.state.lat, this.state.lng);
    await this.addAllMarkers();
  }

  render(){
    return (
      <div style={{minHeight: "calc(100vh - 35px)"}}>
        <div
          id="google-map"
          ref={this.googleMapRef}
          style={{width:"100%", height:"400px"}}
        />
        <Button htmlType="submit" onClick={this.onSetPosition} 
                type="primary" style={{margin:'10px'}}>
          Задать свою позицию
        </Button>
      </div>
    )
  };
}
