import React,{Component} from "react";
import {Link } from "react-router-dom";
import { Icon, Button, Input, List, Avatar, Tooltip } from 'antd';
import FriendRequests from '../requests/friendRequests';
import 'antd/dist/antd.css';
import './css/friends.css';
import {ServerAddress} from '../auth';
const { Search } = Input;

export default class SearchFriends extends Component {
  constructor (props) {
    super(props);
    this.state = {};
    this.state.searchValue = "";
  }
  async componentDidMount() {
    await this.updateFriendList("");
  }
  updateFriendList = async (value)=> {
    let res = await FriendRequests.search(value);
    if (res === false) {
        return;
    }
    this.setState({list:res});
  }
  onSearch = async (e)=>{
    this.setState({[e.target.name]:e.target.value});
    // Here send login request
    await this.updateFriendList(e.target.value);
  }
  sendFriendRequest = async (id) => {
    await FriendRequests.sendFriendRequest(id);
    await this.updateFriendList(this.state.searchValue);
  }
  sendDeleteFriendRequest = async (id) => {
    await FriendRequests.sendDeleteFriendRequest(id);
    await this.updateFriendList(this.state.searchValue);
  }
  getButton = (item) => {
    const addToFriend = (
        <Button type="primary" onClick={() => this.sendFriendRequest(item.id)}>
          <Icon type="user-add" /> 
          Добавить в друзья
        </Button>
      );
    const requestAllreadySend = (
      <Tooltip placement="top" title='Отозвать заявку'>
        <Button onClick={() => this.sendDeleteFriendRequest(item.id)}>
          <Icon type="user-add" /> 
          Заявка отправлена
        </Button>
      </Tooltip>
    );
    const acceptFriendship = (
      <div>
        <Tooltip placement="top" title='Принять дружбу'>
          <Button onClick={() => this.sendFriendRequest(item.id)}>
            <Icon type="user-add" /> 
            Получено приглашение
          </Button>
        </Tooltip>
        <Button type="danger" onClick={() => this.sendDeleteFriendRequest(item.id)}>
          <Icon type="close-circle" /> 
          Отклонить дружбу
        </Button>
      </div>
    );
    const friends = (
      <Tooltip placement="top" title='Отменить дружбу'>
        <Button onClick={() => this.sendDeleteFriendRequest(item.id)}>
          <Icon type="user" /> 
          В друзьях
        </Button>
      </Tooltip>
    );
    if (item.Firsts.length === 0 && item.Seconds.length === 0)
      return addToFriend;
    if (item.Firsts.length === 1 && item.Firsts[0].friend.accepted === false)
      return acceptFriendship;
    if (item.Seconds.length === 1 && item.Seconds[0].friend.accepted === false)
      return requestAllreadySend;
    if ((item.Firsts.length  === 1 && item.Firsts[0].friend.accepted  === true) ||
        (item.Seconds.length === 1 && item.Seconds[0].friend.accepted === true))
      return friends;

    return requestAllreadySend;
  }
  render(){
    const { list } = this.state;
    return (
      <div style={{minHeight: "calc(100vh - 35px)"}}>
        <Search placeholder="Начните вводить почту пользователя"
                name="searchValue"
                onSearch={this.onSearch}
                onChange={this.onSearch}
                style={{ width: '100%', padding:'10px' }}/>
        <List itemLayout="vertical"
              dataSource={list}
              renderItem={item => (
          <List.Item>
            <Link to={"/searchfriends/" + item.id}>
              <List.Item.Meta
                avatar={<Avatar src={ServerAddress + item.avatar + "_small"} />}
                title={item.firstname+" "+item.secondname}
                description={item.email}
              />
            </Link>
            {this.getButton(item)}
          </List.Item>
          )}
        />
      </div>
    )
  };
}