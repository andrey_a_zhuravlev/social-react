import React,{Component} from "react";
import { List, Avatar } from 'antd';
import FriendRequests from '../requests/friendRequests';
import { Link } from "react-router-dom";
import {ServerAddress} from '../auth';

export default class Chatlist extends Component {
  constructor (props) {
    super(props);
    this.state = {};
  }
  async componentDidMount() {
    let res = await FriendRequests.getList();
    if (res !== false)
      this.setState({list:res});
  }
  render(){
    const { list } = this.state;
    return (
      <div style={{minHeight: "calc(100vh - 35px)"}}>
        <List
          header='Список чатов'
          itemLayout="vertical"
          dataSource={list}
          renderItem={item => (
            <List.Item>
              <Link to={"/chatlist/" + item.id}>
                <List.Item.Meta
                  avatar={<Avatar src={ServerAddress + item.avatar+"_small"} />}
                  title={item.firstname+" "+item.secondname}
                  description={item.email}
                />
              </Link>
            </List.Item>
          )}
        />
      </div>
    )
  };
}
