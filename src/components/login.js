import React,{Component} from "react";
import 'antd/dist/antd.css';
import './css/login.css'
import Auth from "../auth";
import { Button, Input, Form, Typography } from 'antd';
import {FacebookLoginButton} from 'react-social-login-buttons';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props'
import AccountRequests from '../requests/accountRequests';

export default class Login extends Component {
    constructor (props) {
        super(props);
        this.state = {
            email: "",
            password: ""};
    }
    onInputChange = (e)=>{
        this.setState({[e.target.name]:e.target.value});
        //console.log(`onInputChange ${this.state.email}, ${this.state.password}`);
    }
    onLogin = async (e)=>{
        e.preventDefault();
        // Here send login request
        let res = await AccountRequests.login(this.state.email, this.state.password);
        if (res === false) {
            return alert("Sorry those credentials don't exist!");
        }
        this.props.history.replace('/');
    }
    componentDidMount(){
        //if logedin allready 
        if (Auth.loggedIn())
            this.props.history.replace('/');
    }
    responseFacebook = async (response) => {
        console.log("responseFacebook ", response);
        let res = await AccountRequests.fblogin(response.accessToken);
        if (res === false) {
            return alert("Error with Facebook login!");
        }
        this.props.history.replace('/');
    }
    render(){
        return (
            <Form className="login-form">
                <h4 className="text-center"> Sign in to </h4>
                <h3 className="text-center"> Social network </h3>
                <Typography>Email</Typography>
                <Input type="email" className="login-form-input" name="email" placeholder="Your Email" onChange={this.onInputChange}/>
                <Typography>Password</Typography>
                <Input type="password" className="login-form-input" name="password" placeholder="Your password" onChange={this.onInputChange}/>
                <Form.Item>
                    <Button type="primary" className="login-form-button" onClick={this.onLogin} block>
                        Log In
                    </Button>
                </Form.Item>
                <div className="text-center">
                    Enter with Your social account:
                </div>
                <FacebookLogin
                    appId="517607039064422"
                    autoLoad={false}
                    fields="name,email,picture"
                    callback={this.responseFacebook}
                    render={renderProps => (
                        <FacebookLoginButton className="bt-3 mb-3" onClick={renderProps.onClick}/>
                    )}
                    />
                
                <div className="text-center">
                    <a href="/signup">Create account</a>
                </div>
            </Form>
        );
    }
}