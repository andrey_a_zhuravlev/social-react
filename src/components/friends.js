import React,{Component} from "react";
import { Link } from "react-router-dom";
import {  Icon, Button, List, Avatar, Tooltip } from 'antd';
import FriendRequests from '../requests/friendRequests';
import 'antd/dist/antd.css';
import './css/friends.css';
import {ServerAddress} from '../auth';

export default class Friends extends Component {
  constructor (props) {
    super(props);
    this.state = {};
  }
  async componentDidMount() {
    let res = await FriendRequests.getList();
    if (res !== false)
      this.setState({list:res});
  }
  sendFriendRequest = async (id) => {
    await FriendRequests.sendFriendRequest(id);
    let res = await FriendRequests.getList();
    if (res !== false)
      this.setState({list:res});
  }
  sendDeleteFriendRequest = async (id) => {
    await FriendRequests.sendDeleteFriendRequest(id);
    let res = await FriendRequests.getList();
    if (res !== false)
      this.setState({list:res});
  }
  getButton = (item) => {
    const requestAllreadySend = (
      <Tooltip placement="top" title='Отозвать заявку'>
        <Button onClick={() => this.sendDeleteFriendRequest(item.id)}>
          <Icon type="user-add" /> 
          Заявка отправлена
        </Button>
      </Tooltip>
    );
    const acceptFriendship = (
      <div>
        <Tooltip placement="top" title='Принять дружбу'>
          <Button onClick={() => this.sendFriendRequest(item.id)}>
            <Icon type="user-add" /> 
            Получено приглашение
          </Button>
        </Tooltip>
        <Button type="danger" onClick={() => this.sendDeleteFriendRequest(item.id)}>
          <Icon type="close-circle" /> 
          Отклонить дружбу
        </Button>
      </div>
    );
    const friends = (
      <Tooltip placement="top" title='Отменить дружбу'>
        <Button onClick={() => this.sendDeleteFriendRequest(item.id)}>
          <Icon type="user" /> 
          В друзьях
        </Button>
      </Tooltip>
    );
    if (item.friend.accepted === true)
      return friends;
    if (item.id === item.friend.FirstId)
      return acceptFriendship;
    if (item.id === item.friend.SecondId)
      return requestAllreadySend;
    
    return friends;
  }
  render(){
    const { list } = this.state;
    return (
      <div style={{minHeight: "calc(100vh - 35px)"}}>
        <List
          header='Список друзей'
          itemLayout="vertical"
          dataSource={list}
          renderItem={item => (
            <List.Item>
              <Link to={"/friends/" + item.id}>
                <List.Item.Meta
                  avatar={<Avatar src={ServerAddress + item.avatar + "_small"} />}
                  title={item.firstname+" "+item.secondname}
                  description={item.email}
                />
              </Link>
              {this.getButton(item)}
            </List.Item>
          )}
        />
      </div>
    )
  };
}