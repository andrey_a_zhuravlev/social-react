import React,{Component} from "react";
import {List, Input, Button} from 'antd';
import Auth,{ServerChatAddress} from '../auth';
import AccountRequests from '../requests/accountRequests';
import MessageRequests from '../requests/messageRequests';
import 'antd/dist/antd.css';
import './css/chat.css';
import io from "socket.io-client";

export default class Chat extends Component {
  socket = null;
  chatId = null;
  constructor (props) {
    super(props);
    this.state = {list:[]};
  }
  async componentDidMount() {
    const data = await AccountRequests.getInfo(this.props.match.params.id);
    this.socket = io.connect(ServerChatAddress);
    this.chatId = Auth.getId()+"and"+this.props.match.params.id;
    if (this.props.match.params.id < Auth.getId())
      this.chatId = this.props.match.params.id+"and"+Auth.getId();
    this.socket.emit('join', this.chatId);
    this.socket.on('message', this.onMessageReceived);

    var messageList = await MessageRequests.get(this.chatId);
    if (!!messageList === true) {
      this.setState({list: messageList});
    }
    this.scrollToBottom();

    if (!!data === true)
        this.setState(data);
  }
  componentWillUnmount() {
    this.socket.disconnect();
  }
  onChangeInput = (e) => {
    this.setState({[e.target.name]:e.target.value});
  }
  sendMessage = () => {
    this.setState({message: ''});
    this.socket.emit('message', { chatId: this.chatId,
                             fromId: Auth.getId(), 
                             toId: this.props.match.params.id, 
                             message: this.state.message })
  }
  onMessageReceived = (data) => {
    this.state.list.push({fromId: data.fromId, message: data.message});
    this.forceUpdate();
    this.scrollToBottom();
  }
  drawMessage = (fromId, message) => {
    if (fromId === this.props.match.params.id) {
      return (
        <div class="talk-bubble-right round tri-right right-top">
          <div class="talktext-right">
            <p>{message}</p>
          </div>
        </div>
      );
    } else {
      return (
        <div class="talk-bubble-left round tri-right left-top">
          <div class="talktext-left">
            <p>{message}</p>
          </div>
        </div>
      );
    }

  }
  scrollToBottom = () => {
    this.el.scrollIntoView({ behavior: 'smooth' });
  }
  render(){
    const { list } = this.state;
    return (
      <div>
        <p style={{textAlign:"center", background: "LightBlue"}}>{this.state.firstname + " " + this.state.secondname}</p>
        <div style={{height: "calc(100vh - 110px)", overflow: 'auto', paddingLeft: 0, marginLeft: 0}}>
          <List
                size="small"
                dataSource={list}
                renderItem={item => (
              <List.Item style={{borderBottomWidth:0, paddingTop:0,paddingBottom:0}}>
                {this.drawMessage(item.fromId, item.message)}
              </List.Item>
            )}
          />
          <div ref={el => { this.el = el; }} />
        </div>
        <div style={{marginRight:'5px'}}>
          <Input 
                  style={{ width: '75%' }}
                  onChange={this.onChangeInput}
                  onPressEnter={this.sendMessage}
                  name="message"
                  value={this.state.message}
                  placeholder="Напишите сообщение..."/>
          <Button onClick={this.sendMessage} type="primary" style={{ width: '25%', paddingLeft: "2px", paddingRight:"2px" }}>
                  Отправить
          </Button>
        </div>
      </div>
    )
  };
}
